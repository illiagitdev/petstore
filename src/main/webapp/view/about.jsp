<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>About</title>
    <style>
        <%@include file="/view/style.css" %>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div class="show-all, myform" style="width: 600px">
    <h1 align="center">About project</h1>
    <h3>Pet requests</h3>
    <p>##### _Request samples_ <br>
        Upload image to pet with ID (POST:​/pet​/{petId}​/uploadImage uploads an image)<br>
        <%--        https://petstore.swagger.io/v2/pet/{id}}/uploadImage<br>--%>

        Create new pet (POST:​/pet Add a new pet to the store)<br>
        http://petstore.swagger.io/v2/pet{JSONbody}<br>

        Update existing pet(all)(PUT: /pet Update an existing pet)<br>
<%--        http://petstore.swagger.io/v2/pet{JSONbody}<br>--%>

        Get list of pets by status (GET​/pet​/findByStatus Finds Pets by status)<br>
        http://petstore.swagger.io/v2/pet/findByStatus?status={status} {available, pending, sold}<br><br>

        Get pet by {id}(GET:​/pet​/{petId} Find pet by ID)<br>
        http://petstore.swagger.io/v2/pet/{id}<br><br>

        Update pet (POST:​/pet​/{petId} Updates a pet in the store with form data)<br>
        http://petstore.swagger.io/v2/pet{formData}<br>

        Delete pet (DELETE:​/pet​/{petId} Deletes a pet)<br>
        http://petstore.swagger.io/v2/pet/{id}<br>
    </p>
    <h3>Store requests</h3>
    <p>##### _Request samples_ <br>
        Create new order (POST: /store​/order{JSONbody} Place an order for a pet)<br>
        http://petstore.swagger.io/v2/store/order{JSONbody}<br>

        Find order by id (GET: /store​/order​/{orderId} Find purchase order by ID)<br>
        http://petstore.swagger.io/v2/store/order/{id}<br>

        Delete order (DELETE: /store​/order​/{orderId} Delete purchase order by ID)<br>
        http://petstore.swagger.io/v2/store/order/{id}<br>

        Get orders inventory (GET: /store​/inventory Returns pet inventories by status)<br>
        http://petstore.swagger.io/v2/store/inventory<br>
    </p>
    <h3>User requests</h3>
    <p>##### _Request samples_ <br>
        Create new user (POST: /user{JSONbody} Create user)<br>
        http://petstore.swagger.io/v2/user/admin<br>

        Find user by username(PUT: /user​/{username} Updated user)<br>
        http://petstore.swagger.io/v2/user/{username}<br>

        Delete user (DELETE: ​/user​/{username} Delete user)<br>
        http://petstore.swagger.io/v2/user/{username}<br>

        Update user (PUT: ​/user​/{username} Updated user)<br>
        http://petstore.swagger.io/v2/user/{username} {JSONbody}<br>

        Log in user (GET: ​/user​/login Logs user into the system)<br>
        http://petstore.swagger.io/v2/user/login?username=username&password=password<br>

        Log out user (GET: ​/user​/logout Logs out current logged in user session)<br>
        <br>
    </p>
</div>
</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update category</title>
    <style>
        <%@include file="/view/style.css" %>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="updateCategory">
        <h1>Update category</h1>
        <input type="number" name="id" id="id" value="${id}" hidden/>
        <c:choose>
            <c:when test="${not empty category}">
                <label>Category id
                    <span class="small">Enter id</span>
                </label>
                <input type="number" name="categoryId" id="categoryId" value="${category.id}"/>
                <label>Category name
                    <span class="small">Enter name</span>
                </label>
                <input type="text" name="categoryName" id="categoryName" value="${category.name}"/>
                <button type="submit" class="button">Update</button>
                <a href="${pageContext.request.contextPath}/pet/deleteCategory?id=${id}" class="button" role="button">Delete</a>
            </c:when>
            <c:otherwise>
                <label>Category id
                    <span class="small">Enter id</span>
                </label>
                <input type="number" name="categoryId" id="categoryId"/>
                <label>Category name
                    <span class="small">Enter name</span>
                </label>
                <input type="text" name="categoryName" id="categoryName"/>
                <button type="submit" class="button">Add</button>
            </c:otherwise>
        </c:choose>
        <c:if test="">
            </c:if>
    </form>
</div>
</body>
</html>

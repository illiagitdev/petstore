<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <title>Course Management</title>
</head>
<body>
<div class="navbar">
    <a href="${pageContext.request.contextPath}/">Home</a>
    <div class="dropdown">
        <button class="dropbtn">Pet
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="${pageContext.request.contextPath}/pet/findPetByStatus">Find Pets By Status</a>
            <a href="${pageContext.request.contextPath}/pet/findPetById">Find Pet By Id</a>
            <a href="${pageContext.request.contextPath}/pet/createPet">Add a New Pet</a>
        </div>
    </div>
    <div class="dropdown">
        <button class="dropbtn">Store
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="${pageContext.request.contextPath}/order/createOrder">Place Order</a>
            <a href="${pageContext.request.contextPath}/order/findOrder">Find Order</a>
            <a href="${pageContext.request.contextPath}/order/inventory">Show Inventory</a>
        </div>
    </div>
    <div class="dropdown">
        <button class="dropbtn">User
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="${pageContext.request.contextPath}/user/createUser">Create User</a>
            <a href="${pageContext.request.contextPath}/user/findUser">Find User</a>
        </div>
    </div><div class="dropdown">
        <button class="dropbtn">Authorization
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
            <a href="${pageContext.request.contextPath}/authorization/login">Log In</a>
            <a href="${pageContext.request.contextPath}/authorization/logout">Log out</a>
        </div>
    </div>
    <a href="${pageContext.request.contextPath}/about">About</a>
</div></body>
</html>
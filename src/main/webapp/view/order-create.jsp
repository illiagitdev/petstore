<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create order</title>
    <style><%@include file="/view/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="createOrder">
        <h1>Create order</h1>

        <label>Id
            <span class="small">Id</span>
        </label>
        <input type="number" name="id" id="id" />

        <label>Pet id
            <span class="small">Id</span>
        </label>
        <input type="text" name="petId" id="petId" />

        <label>Quantity
            <span class="small">Quantity</span>
        </label>
        <input type="text" name="quantity" id="quantity" />

        <label>Order ship date
            <span class="small">Ship date</span>
        </label>
        <input type="date" name="shipDate" id="shipDate" />

        <label>Select status
            <span class="small">Status</span>
        </label>
        <select name="status" id="status">
            <option value="placed">Placed</option>
            <option value="delivered">Delivered</option>
            <option value="revoked" hidden>Revoked</option>
        </select>

        <label>Complete state
            <span class="small">Complete state</span>
        </label>
        <select  name="complete" id="complete">
            <option value="true">True</option>
            <option value="false">False</option>
        </select>

        <button type="submit">Create</button>
        <div class="spacer"></div>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <p style="color: red">${error}</p>
            </c:forEach>
        </c:if>
    </form>
</div>
</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update pet</title>
    <style><%@include file="/view/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div id="stylized" class="myform">
    <c:choose>
        <c:when test="${not empty pet}">
        <form id="form" name="form" method="post" action="updatePet">
            <h1>Update pet</h1>

            <input type="number" name="id" id="id" value="${pet.id}" hidden />

            <label>Pet name
                <span class="small">Enter name</span>
            </label>
            <input type="text" name="name" id="name"  value="${pet.name}"/>

            <label>Select status
                <span class="small">${pet.status}*</span>
            </label>
            <select name="status" id="status">
                <option value="available">Available</option>
                <option value="pending">Pending</option>
                <option value="sold">Sold</option>
            </select>
            <button type="submit" class="button">Update</button>
            <c:if test="${not empty errors}">
                <c:forEach items="${errors}" var="error">
                    <p style="color: red">${error}</p>
                </c:forEach>
            </c:if>
        </form>
        </c:when>
        <c:otherwise><h1>Pet wasn't found</h1></c:otherwise>
    </c:choose>
</div>
</body>
</html>

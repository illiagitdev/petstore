<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Petstore Home</title>
    <style>
        <%@include file="/view/style.css"%>
    </style>
</head>
<body>
    <c:import url="/view/navigation-bar.jsp"/>
    <div class="myform, show-all">
    <h1>Hello to store</h1>
    <p>
        Proceed to dropdown menu and select option you need!<br/>
        Have fun :)
    </p>
    </div>
</body>
</html>

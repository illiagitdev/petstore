<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create pet</title>
    <style><%@include file="/view/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="createPet">
        <h1>Create pet</h1>

        <label>Id
            <span class="small">Id</span>
        </label>
        <input type="number" name="id" id="id" />

        <label>Pet name
            <span class="small">Enter name</span>
        </label>
        <input type="text" name="name" id="name" />

        <label>Select status
            <span class="small">Status</span>
        </label>
        <select name="status" id="status">
            <option value="available">Available</option>
            <option value="pending">Pending</option>
            <option value="sold">Sold</option>
        </select>

        <label>Category Name
            <span class="small">Category name</span>
        </label>
        <input type="text" name="categoryName" id="categoryName" />

        <label>Category id
            <span class="small">Id</span>
        </label>
        <input type="number" name="categoryId" id="categoryId" />

        <label>Photo url
            <span class="small">Photo url</span>
        </label>
        <input type="text" name="photoUrl" id="photoUrl" />

        <label>Tag Name
            <span class="small">Tag name</span>
        </label>
        <input type="text" name="tagName" id="tagName" />

        <label>Tag id
            <span class="small">Id</span>
        </label>
        <input type="number" name="tadId" id="tadId" />

        <button type="submit">Create</button>
        <div class="spacer"></div>

        <c:if test="${not empty errors}">
            <c:forEach items="${errors}" var="error">
                <p style="color: red">${error}</p>
            </c:forEach>
        </c:if>
    </form>
</div>
</body>
</html>

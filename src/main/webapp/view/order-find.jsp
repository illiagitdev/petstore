<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Find order</title>
    <style>
        <%@include file="/view/style.css" %>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div  id="stylized" class="myform">
    <form id="form" name="form" method="get" action="findOrder">
        <h1>Search order by id.</h1>
        <label>Enter order id:
            <span class="small">ID</span>
        </label>
        <input type="number" id="id" name="id"/>
        <button type="submit">Find</button>
        <div class="spacer"></div>
        <c:if test="${not empty error}">
                <p style="color: red">${error}</p>
        </c:if>
    </form>

    <c:if test="${not empty order}">
    <p>Order details</p>
    <table class="zui-table">
        <tbody>
        <tr>
            <td>Pet id:</td>
            <td>${order.petId}</td>
        </tr>
        <tr>
            <td>Quantity:</td>
            <td>${order.quantity}</td>
        </tr>
        <tr>
            <td>Ship date:</td>
            <td>${order.shipDate}</td>
        </tr>
        <tr>
            <td>Order status:</td>
            <td>${order.status}</td>
        </tr>
        <tr>
            <td>Order complete:</td>
            <td>${order.complete}</td>
        </tr>
        </tbody>
    </table>
    <a href="${pageContext.request.contextPath}/order/deleteOrder?id=${order.id}" class="button" role="button">Delete</a>
</div>
</c:if>
</body>
</html>

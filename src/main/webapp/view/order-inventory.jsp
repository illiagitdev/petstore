<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Inventory</title>
    <style>
        <%@include file="/view/style.css" %>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div id="stylized" class="myform">
    <c:if test="${not empty inventory}">
        <p>Orders inventory</p>
        <table class="zui-table">
            <tbody>
            <tr>
                <td>Sold</td>
                <td>${inventory.sold}</td>
            </tr><tr>
                <td>String</td>
                <td>${inventory.string}</td>
            </tr><tr>
                <td>Pending</td>
                <td>${inventory.pending}</td>
            </tr><tr>
                <td>Available</td>
                <td>${inventory.available}</td>
            </tr>
            </tbody>
        </table>
    </c:if>
    <c:if test="${not empty error}">
        <p style="color: red">${error}</p>
    </c:if>
</div>
</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update tags</title>
    <style>
        <%@include file="/view/style.css" %>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div id="stylized" class="myform">
    <h1>Add new tags</h1>
    <%--    ADD category --%>
    <form id="form" name="form" method="post" action="addTag">
        <input type="number" name="id" id="id" value="${id}" hidden/>
        <label>Tag id
            <span class="small">Id</span>
        </label>
        <input type="number" name="tagId" id="tagIdNew"/>
        <label>Tag name
            <span class="small">Enter name</span>
        </label>
        <input type="text" name="tagName" id="tagNameNew"/>
        <button type="submit" class="button">Add tag</button>
    </form>
    <%--  DELETE --%>
    <c:if test="${not empty tags}">
        <p>Tags list</p>
        <table class="zui-table">
            <tbody>
            <tr>
                <td>Tag id:</td>
                <td>Tag name:</td>
            </tr>
            <c:forEach items="${tags}" var="tag">
                <tr>
                    <td>${tag.id}</td>
                    <td>${tag.name}</td>
                    <td><a href="${pageContext.request.contextPath}/pet/deleteTag?id=${id}&tagId=${tag.id}" class="button"
                           role="button">Delete</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>
</body>
</html>

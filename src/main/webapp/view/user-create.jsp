<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create user</title>
    <style><%@include file="/view/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="createUser">
        <h1>Create user</h1>

        <label>Id
            <span class="small">Id</span>
        </label>
        <input type="number" name="id" id="id" />

        <label>User nickname
            <span class="small">Enter nickname</span>
        </label>
        <input type="text" name="username" id="username" />

        <label>User first name
            <span class="small">Enter name</span>
        </label>
        <input type="text" name="firstName" id="firstName" />

        <label>User last name
            <span class="small">Enter name</span>
        </label>
        <input type="text" name="lastName" id="lastName" />

        <label>User email
            <span class="small">Enter email</span>
        </label>
        <input type="text" name="email" id="email" />

        <label>User phone number
            <span class="small">Enter phone</span>
        </label>
        <input type="text" name="phone" id="phone" />

        <label>User password
            <span class="small">Enter password</span>
        </label>
        <input type="password" name="password" id="password" />

        <button type="submit">Create</button>
        <div class="spacer"></div>

        <c:if test="${not empty errors}">
            <c:forEach items="errors" var="error">
                <p style="color: red">${error}</p>
            </c:forEach>
        </c:if>
    </form>
</div>
</body>
</html>

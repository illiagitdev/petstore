<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Found Pets</title>
    <style><%@include file="/view/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div  id="stylized" class="myform">
    <form id="form" name="form" method="get" action="findPetByStatus">
        <h1>Select status to see all orders:</h1>
        <label>Available Options
            <span class="small">Select</span>
        </label>
            <select id="status" name="status">
                <option value="available">Available</option>
                <option value="pending">Pending</option>
                <option value="sold">Sold</option>
            </select>
    <button type="submit">Find</button>
    <div class="spacer"></div>
    </form>
    <c:if test="${not empty pets}">
    <h3>Found pets with status: <c:out value="${status}"/></h3>
        <table class="zui-table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${pets}" var="pet">
                <tr>
                    <td><c:out value="${pet.id}"/></td>
                    <td><c:out value="${pet.name}"/></td>
                    <td><c:out value="${pet.status}"/></td>
                    <td>
                        <a href="${pageContext.request.contextPath}/pet/petDetails?id=${pet.id}" class="button" role="button" tabindex="0">Details</a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>

</div>

</body>
</html>

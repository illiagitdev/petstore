<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>pet Details</title>
    <style>
        <%@include file="/view/style.css" %>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div class="zui-table, show-all">
    <p>Pet details</p>
    <c:if test="${not empty pet}">
        <table class="zui-table">
            <tbody>
            <tr>
                <td>Pet name:</td>
                <td>${pet.name}</td>
            </tr>
            <tr>
                <td>Pet status:</td>
                <td>${pet.status}</td>
            </tr>
            <c:if test="${not empty pet.category}">
                <tr>
                    <td>Pet category</td>
                    <td>${pet.category.name}</td>
                </tr>
            </c:if>
            <c:if test="${not empty pet.tags}">
                <c:forEach items="${pet.tags}" var="tags">
                    <tr>
                        <td>Pet tags:</td>
                        <td>${tags.name}</td>
                    </tr>
                </c:forEach>
            </c:if>
            <c:if test="${not empty pet.photoUrls}">
                <c:forEach items="${pet.photoUrls}" var="photoUrls">
                    <tr>
                        <td>Pet image url:</td>
                        <td>${photoUrls}</td>
                    </tr>
                </c:forEach>
            </c:if>
            </tbody>
        </table>
    </c:if>

    <c:if test="${not empty pet}">
        <a href="${pageContext.request.contextPath}/pet/updatePet?id=${pet.id}" class="button" role="button">Update</a>
        <a href="${pageContext.request.contextPath}/pet/uploadImagePet?id=${pet.id}" class="button" role="button">Update
            image</a>
        <a href="${pageContext.request.contextPath}/pet/deletePet?id=${pet.id}" class="button" role="button">Delete</a>
        <a href="${pageContext.request.contextPath}/pet/updateCategory?id=${pet.id}" class="button" role="button">Update
            category</a>
        <a href="${pageContext.request.contextPath}/pet/updateTag?id=${pet.id}" class="button" role="button">Update
            tag</a>
    </c:if>
</div>
</body>
</html>

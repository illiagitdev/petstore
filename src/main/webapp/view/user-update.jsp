<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update user</title>
    <style><%@include file="/view/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div id="stylized" class="myform">
    <c:choose>
        <c:when test="${not empty user}">
            <form id="form" name="form" method="post" action="updateUser">
                <h1>Update user</h1>

                <input type="text" name="oldUsername" id="oldUsername" value="${oldUsername}" hidden />

                <label>Id
                    <span class="small">Id</span>
                </label>
                <input type="number" name="id" id="id" value="${user.id}"/>

                <label>User nickname
                    <span class="small">Enter nickname</span>
                </label>
                <input type="text" name="username" id="username" value="${user.username}"/>

                <label>User first name
                    <span class="small">Enter name</span>
                </label>
                <input type="text" name="firstName" id="firstName"  value="${user.firstName}"/>

                <label>User last name
                    <span class="small">Enter name</span>
                </label>
                <input type="text" name="lastName" id="lastName" value="${user.lastName}"/>

                <label>User email
                    <span class="small">Enter email</span>
                </label>
                <input type="text" name="email" id="email"  value="${user.email}"/>

                <label>User phone number
                    <span class="small">Enter phone</span>
                </label>
                <input type="text" name="phone" id="phone"  value="${user.phone}"/>

                <label>User password
                    <span class="small">Enter password</span>
                </label>
                <input type="password" name="password" id="password" />

                <button type="submit">Update</button>
                <div class="spacer"></div>

                <c:if test="${not empty errors}">
                    <c:forEach items="errors" var="error">
                        <p style="color: red">${error}</p>
                    </c:forEach>
                </c:if>
            </form>
        </c:when>
        <c:otherwise><h1>User wasn't found</h1></c:otherwise>
    </c:choose>
</div>
</body>
</html>

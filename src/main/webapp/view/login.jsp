<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Log In</title>
    <style><%@include file="/view/style.css"%></style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div id="stylized" class="myform">
    <form id="form" name="form" method="post" action="login">
        <h1>Log In</h1>

        <label>Username</label>
        <input type="text" name="username" id="username" />

        <label>Password</label>
        <input type="password" name="password" id="password" />

        <button type="submit">Log In</button>
        <div class="spacer"></div>

        <c:if test="${not empty error}">
                <p style="color: red">${error}</p>
        </c:if>
    </form>
</div>
</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Find user</title>
    <style>
        <%@include file="/view/style.css" %>
    </style>
</head>
<body>
<c:import url="/view/navigation-bar.jsp"/>
<div  id="stylized" class="myform">
    <form id="form" name="form" method="get" action="findUser">
        <h1>Search user by username.</h1>
        <label>Enter user username:
            <span class="small">Username</span>
        </label>
        <input type="text" id="username" name="username"/>
        <button type="submit">Find</button>
        <div class="spacer"></div>
        <c:if test="${not empty error}">
            <p style="color: red">${error}</p>
        </c:if>
    </form>

    <c:if test="${not empty user}">
    <p>Order details</p>
    <table class="zui-table">
        <tbody>
        <tr>
            <td>Username:</td>
            <td>${user.username}</td>
        </tr>
        <tr>
            <td>First Name:</td>
            <td>${user.firstName}</td>
        </tr>
        <tr>
            <td>Last Name:</td>
            <td>${user.lastName}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>${user.email}</td>
        </tr>
        <tr>
            <td>Phone number:</td>
            <td>${user.phone}</td>
        </tr>
        <tr>
            <td>User Status:</td>
            <td>${user.userStatus}</td>
        </tr>
        </tbody>
    </table>
    <a href="${pageContext.request.contextPath}/user/deleteUser?username=${user.username}" class="button" role="button">Delete</a>
    <a href="${pageContext.request.contextPath}/user/updateUser?username=${user.username}" class="button" role="button">Update</a>
</div>
</c:if>
</body>
</html>

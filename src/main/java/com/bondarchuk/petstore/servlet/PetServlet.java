package com.bondarchuk.petstore.servlet;

import com.bondarchuk.petstore.controller.PetController;
import com.bondarchuk.petstore.pojo.Category;
import com.bondarchuk.petstore.pojo.Pet;
import com.bondarchuk.petstore.pojo.Tags;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@WebServlet(urlPatterns = "/pet/*")
public class PetServlet extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(PetServlet.class);
    private static PetController petController;

    @Override
    public void init() throws ServletException {
        super.init();
        petController = new PetController();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);
        LOG.debug(String.format("doGet: action: %s", action));

        if (action.startsWith("/findPetByStatus")) {
            String status = req.getParameter("status");
            List<Pet> pets = petController.petsByStatus(status);
            LOG.debug(String.format("/findPetByStatus?status=%s", status));
            req.setAttribute("status", status);
            req.setAttribute("pets", pets);
            req.getRequestDispatcher("/view/pet-all-by-status.jsp").forward(req, resp);

        } else if (action.startsWith("/findPetById")) {

            String id = req.getParameter("id");
            if (Objects.isNull(id)) {
                req.getRequestDispatcher("/view/pet-find-id.jsp").forward(req, resp);
            } else {
                Pet pet = petController.getPet(id);
                req.setAttribute("pet", pet);
                LOG.debug(String.format("/findPetById?id=%s", id));
                req.getRequestDispatcher("/view/pet-find-id.jsp").forward(req, resp);
            }
        } else if (action.startsWith("/petDetails")) {

            String id = req.getParameter("id");
            Pet pet = petController.getPet(id);
            LOG.debug(String.format("/petDetails?id=%s", id));
            req.setAttribute("pet", pet);
            req.getRequestDispatcher("/view/pet-details.jsp").forward(req, resp);

        } else if (action.startsWith("/updatePet")) {
            String id = req.getParameter("id");
            Pet pet = petController.getPet(id);
            req.setAttribute("pet", pet);
            LOG.debug(String.format("/updatePet?id=%s", id));
            req.getRequestDispatcher("/view/pet-update.jsp").forward(req, resp);

        } else if (action.startsWith("/createPet")) {

            req.getRequestDispatcher("/view/create-pet.jsp").forward(req, resp);

        } else if (action.startsWith("/deletePet")) {

            String id = req.getParameter("id");
            String message = petController.delete(id);
            if (message != null) {
                req.setAttribute("message", message);
            } else {
                req.setAttribute("message", "Pet deleted");
            }
            LOG.debug(String.format("/deletePet?id=%s", id));
            req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);

        } else if (action.startsWith("/updateCategory")) {
            String id = req.getParameter("id");
            Pet pet = petController.getPet(id);
            req.setAttribute("id", id);
            req.setAttribute("category", pet.getCategory());
            LOG.debug(String.format("/updateCategory id:%s", id));
            req.getRequestDispatcher("/view/pet-update-category.jsp").forward(req, resp);

        } else if (action.startsWith("/deleteCategory")) {
            String id = req.getParameter("id");
            Pet pet = petController.getPet(id);
            pet.setCategory(null);
            Boolean result = petController.updatePet(pet);
            String message;
            if (result) {
                message = "Category deleted";
            } else {
                message = "Error deleting category";
            }
            LOG.debug(String.format("/deleteCategory id:%s", id));
            req.setAttribute("message", message);
            req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);

        } else if (action.startsWith("/updateTag")) {
            String id = req.getParameter("id");
            Pet pet = petController.getPet(id);
            List<Tags> tags = pet.getTags();
            req.setAttribute("id", id);
            if (tags != null) {
                req.setAttribute("tags", tags);
                LOG.debug(String.format("/updateTag id:%s tag.count:%s", id, tags.size()));
            }
            req.getRequestDispatcher("/view/pet-update-tags.jsp").forward(req, resp);

        } else if (action.startsWith("/deleteTag")) {
            String id = req.getParameter("id");
            String tagId = req.getParameter("tagId");
            Pet pet = petController.getPet(id);
            List<Tags> tags = deleteTad(tagId, pet.getTags());
            pet.setTags(tags);
            Boolean aBoolean = petController.updatePet(pet);
            String message;
            if (aBoolean) {
                message = "Tag deleted";
            } else {
                message = "Error deleting tag";
            }
            req.setAttribute("message", message);
            req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
        } else if (action.startsWith("/uploadImagePet")) {
            String id = req.getParameter("id");
            req.setAttribute("id", id);
            req.getRequestDispatcher("/view/upload-image.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);
        LOG.debug(String.format("doPost: action: %s", action));

        if (action.startsWith("/createPet")) {
            Pet pet = petController.mapPet(req);
            List<String> errors = validatePet(pet);

            if (!errors.isEmpty()) {
                req.setAttribute("errors", errors);
                LOG.debug("/createPet errors" + String.join(", ", errors));
                req.getRequestDispatcher("/view/pet-create.jsp").forward(req, resp);
            } else {
                LOG.debug(String.format("Pet added: %s", pet.getName()));
                petController.addPet(pet);
                String message = String.format("Pet %s added!", pet.getName());
                req.setAttribute("message", message);
            }
            req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);

        } else if (action.startsWith("/updatePet")) {
            String id = req.getParameter("id");
            String name = req.getParameter("name");
            String status = req.getParameter("status");
            if (name.trim().isEmpty()) {
                Pet pet = petController.getPet(id);
                req.setAttribute("errors", "No name indicated");
                req.setAttribute("pet", pet);
                LOG.debug("/updatePet errors: No name indicated");
                req.getRequestDispatcher("/view/pet-update.jsp").forward(req, resp);
            } else {
                petController.updatePet(id, name, status);
                String message = String.format("Pet updated with name: %s!", name);
                LOG.debug(String.format("Pet updated: %s", name));
                req.setAttribute("message", message);
                req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
            }
        } else if (action.startsWith("/updateCategory")) {
            String id = req.getParameter("id");

            String categoryId = req.getParameter("categoryId");
            String categoryName = req.getParameter("categoryName");
            Pet pet = petController.getPet(id);
            Category category = new Category();
            category.setId(Long.parseLong(categoryId));
            category.setName(categoryName);
            pet.setCategory(category);

            Boolean result = petController.updatePet(pet);
            String message;
            if (result) {
                message = "Category updated";
            } else {
                message = "Error updating category";
            }
            req.setAttribute("message", message);
            LOG.debug(String.format("/updateCategory id:%s categoryId:%s categoryName:%s, ", id,categoryId, categoryName));
            req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);

        } else if (action.startsWith("/addTag")) {
            String id = req.getParameter("id");
            String tagId = req.getParameter("tagId");
            String tagName = req.getParameter("tagName");
            Pet pet = petController.getPet(id);
            List<Tags> tags = mapTag(tagId, tagName, pet.getTags());
            pet.setTags(tags);
            Boolean aBoolean = petController.updatePet(pet);
            String message;
            LOG.debug(String.format("/addTag pet:%s tag:%s", pet.getName(), tagName));
            if (aBoolean) {
                message = String.format("Tag updated to '%s' ", tagName);
            } else {
                message = "Error updating tag";
            }
            req.setAttribute("message", message);
            req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
        } else if (action.startsWith("/uploadImagePet")) {
            String id = null;
            String message;
            if (ServletFileUpload.isMultipartContent(req)) {
                try {
                    id = req.getParameter("id");
                    List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(req);

                    petController.uploadFile(multiparts, id);
                } catch (Exception e) {
                    message ="File upload fail";
                    req.setAttribute("error", message);
                    req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
                }
            } else {
                message = "File not found";
                req.setAttribute("error", message);
                req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
            }
            resp.sendRedirect(String.format("/petstore/pet/findPetById?id=%s", id));
        }
    }

    private List<Tags> deleteTad(String tagId, List<Tags> tags) {
        tags.removeIf((tags1 -> tags1.getId() == Long.parseLong(tagId)));
        if (tags.size() == 0) {
            return null;
        }
        return tags;
    }

    private List<Tags> mapTag(String tagId, String tagName, List<Tags> tags) {
        if (Objects.isNull(tags)){
            tags = new ArrayList<>();
        }
        Tags tag = new Tags();
        tag.setName(tagName);
        tag.setId(Long.parseLong(tagId));
        tags.add(tag);
        return tags;
    }

    private List<String> validatePet(Pet pet) {
        List<String> errors = new ArrayList<>();
        if (pet.getName().trim().isEmpty()) {
            errors.add("No name indicated");
        }
        if (petController.checkId(pet.getId())) {
            errors.add(String.format("Pet with id=%s already exists", pet.getId()));
        }
        return errors;
    }

    private String getAction(HttpServletRequest req) {
        String requestURI = req.getRequestURI();
        String requestPathWithServletContext = req.getContextPath() + req.getServletPath();
        return requestURI.substring(requestPathWithServletContext.length());
    }
}

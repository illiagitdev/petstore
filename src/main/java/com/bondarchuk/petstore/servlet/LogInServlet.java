package com.bondarchuk.petstore.servlet;

import com.bondarchuk.petstore.controller.UserController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

@WebServlet(urlPatterns = "/authorization/*")
public class LogInServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LogManager.getLogger(LogInServlet.class);
    private UserController userController;

    @Override
    public void init() throws ServletException {
        super.init();
        userController = new UserController();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);
        HttpSession session = req.getSession(false);
        LOG.debug(String.format("doGet: action: %s", action));
        if (action.startsWith("/login")) {
            req.getRequestDispatcher("/view/login.jsp").forward(req, resp);
        } else if (action.startsWith("/logout")) {
            if (session != null && session.getAttribute("sessionUser") != null) {
                session.invalidate();
                userController.logout();
            }
            String message = "You are logged out.";
            req.setAttribute("message", message);
            LOG.debug(String.format("doGet: /logout: %s", message));
            req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);
        LOG.debug(String.format("doPost: action: %s", action));

        String message;
        if (action.startsWith("/login")) {
            String username = req.getParameter("username");
            String password = req.getParameter("password");
            Boolean isUser = userController.isExists(username);
            HttpSession session = req.getSession(false);
            if (!isUser) {
                message = "No user with such username";
                LOG.debug(String.format("doPost: /login: %s", message));
                req.setAttribute("error", message);
                req.getRequestDispatcher("/view/login.jsp").forward(req, resp);
            } else if (session != null && session.getAttribute("sessionUser") != null) {
                message = String.format("User '%s' logged in right now", session.getAttribute("sessionUser"));
                req.setAttribute("message", message);
                LOG.debug(String.format("doPost: /login: %s", message));
                req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
            } else {
                Boolean login = userController.login(username, password);
                if (login) {
                    session = req.getSession();
                    session.setAttribute("sessionUser", username);
                    message = String.format("Welcome %s!", username);
                    req.setAttribute("message", message);
                    LOG.debug(String.format("doPost: /login: %s", message));
                    req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
                } else {
                    message = "Wrong password";
                    LOG.debug(String.format("doPost: /login: %s", message));
                    req.setAttribute("error", message);
                    req.getRequestDispatcher("/view/login.jsp").forward(req, resp);
                }
            }
        }
    }

    private String getAction(HttpServletRequest req) {
        String requestURI = req.getRequestURI();
        String requestPathWithServletContext = req.getContextPath() + req.getServletPath();
        return requestURI.substring(requestPathWithServletContext.length());
    }
}

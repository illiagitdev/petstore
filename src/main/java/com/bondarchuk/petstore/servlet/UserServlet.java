package com.bondarchuk.petstore.servlet;

import com.bondarchuk.petstore.controller.UserController;
import com.bondarchuk.petstore.pojo.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@WebServlet(urlPatterns = "/user/*")
public class UserServlet extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(UserServlet.class);
    private UserController userController;

    @Override
    public void init() throws ServletException {
        super.init();
        userController = new UserController();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);
        LOG.debug(String.format("doGet: action: %s", action));

        if (action.startsWith("/createUser")) {
            req.getRequestDispatcher("/view/user-create.jsp").forward(req, resp);
        } else if (action.startsWith("/findUser")) {
            String username = req.getParameter("username");
            if (Objects.nonNull(username)) {
                User user = userController.getUser(username);
                if (user != null) {
                    req.setAttribute("user", user);
                } else {
                    String error = String.format("user with username=%s not found!", username);
                    req.setAttribute("error", error);
                }
            }
            req.getRequestDispatcher("/view/user-find.jsp").forward(req, resp);
        } else if (action.startsWith("/deleteUser")) {
            String username = req.getParameter("username");
            Boolean delete = userController.deleteUser(username);
            if (delete) {
                req.setAttribute("message", "User deleted!");
            } else {
                req.setAttribute("message", "Cant delete user, try later!");
            }
            req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
        } else if (action.startsWith("/updateUser")){
            String username = req.getParameter("username");
            User user = userController.getUser(username);
            req.setAttribute("user", user);
            req.setAttribute("oldUsername", username);
            req.getRequestDispatcher("/view/user-update.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);
        LOG.debug(String.format("doGet: action: %s", action));

        if (action.startsWith("/createUser")) {
            User user = userController.mapUser(req);
            List<String> errors = validateUser(user);
            if (!errors.isEmpty()) {
                req.setAttribute("errors", errors);
                LOG.debug("/createUser error" + errors);
                req.getRequestDispatcher("/view/user-create.jsp").forward(req, resp);
            } else {
                Boolean isCreated = userController.addUser(user);
                String message;
                if (isCreated) {
                    message = String.format("User %s added!", user.getUsername());
                } else {
                    message = String.format("Error creating user %s!", user.getUsername());
                }
                LOG.debug(message);
                req.setAttribute("message", message);
            }
            req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
        }else if (action.startsWith("/updateUser")) {
            User user = userController.mapUser(req);
            String oldUsername = req.getParameter("oldUsername");
            List<String> errors = validateUser(user);
            if (!errors.isEmpty()) {
                req.setAttribute("errors", errors);
                req.setAttribute("user", user);
                req.setAttribute("oldUsername", oldUsername);
                LOG.debug("/updateUser errors:" + String.join(",", errors));
                req.getRequestDispatcher("/view/user-update.jsp").forward(req, resp);
            } else {
                Boolean result = userController.updateUser(oldUsername, user);
                String message;
                if (result){
                    message = String.format("User updated: %s - %s!", oldUsername, user.getUsername());
                } else {
                    message = "User wasn't updated";
                }
                LOG.debug(message);
                req.setAttribute("message", message);
                req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
            }
        }
    }

    private List<String> validateUser(User user) {
        List<String> errors = new ArrayList<>();
        if (user.getId() <= 0) {
            errors.add("Order id cant be less or equal 0");
        } else {
            User userExists = userController.getUser(user.getUsername());
            if (userExists != null) {
                errors.add(String.format("User with such username=%s already exists", user.getUsername()));
            }
        }
        if (user.getUsername().trim().isEmpty()) {
            errors.add("Username cant be empty");
        } else if (user.getUsername().length() < 5 && user.getUsername().length() > 15) {
            errors.add("Username is too long(>5) or short(<15)");
        }
        return errors;
    }

    private String getAction(HttpServletRequest req) {
        String requestURI = req.getRequestURI();
        String requestPathWithServletContext = req.getContextPath() + req.getServletPath();
        return requestURI.substring(requestPathWithServletContext.length());
    }
}

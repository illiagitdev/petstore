package com.bondarchuk.petstore.servlet;

import com.bondarchuk.petstore.controller.OrderController;
import com.bondarchuk.petstore.controller.PetController;
import com.bondarchuk.petstore.pojo.Inventory;
import com.bondarchuk.petstore.pojo.Order;
import com.bondarchuk.petstore.pojo.Pet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@WebServlet(urlPatterns = "/order/*")
public class OrderServlet extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger(PetServlet.class);
    private OrderController orderController;

    @Override
    public void init() throws ServletException {
        super.init();
        orderController = new OrderController();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);
        LOG.debug(String.format("doGet: action: %s", action));

        if (action.startsWith("/findOrder")) {
            String id = req.getParameter("id");
            if (Objects.nonNull(id)) {
                Order order = orderController.getOrder(id);
                if (order != null) {
                    req.setAttribute("order", order);
                } else {
                    String error = String.format("Order with id=%s not found!", id);
                    req.setAttribute("error", error);
                }
            }
            req.getRequestDispatcher("/view/order-find.jsp").forward(req, resp);
        } else if (action.startsWith("/deleteOrder")) {
            String id = req.getParameter("id");
            Boolean delete = orderController.deleteOrder(id);
            if (delete) {
                req.setAttribute("message", "Order deleted!");
            } else {
                req.setAttribute("message", "Cant delete order, try later!");
            }
            req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
        } else if (action.startsWith("/inventory")) {
            Inventory inventory = orderController.getInventory();
            if (Objects.nonNull(inventory)) {
                req.setAttribute("inventory", inventory);
            } else {
                String error = "Inventory not found!";
                req.setAttribute("error", error);
            }
            req.getRequestDispatcher("/view/order-inventory.jsp").forward(req, resp);
        } else if (action.startsWith("/createOrder")) {
            req.getRequestDispatcher("/view/order-create.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = getAction(req);
        if (action.startsWith("/createOrder")) {
            Order order = orderController.mapOrder(req);
            List<String> errors = validateOrder(order);

            if (!errors.isEmpty()) {
                req.setAttribute("errors", errors);
                LOG.debug("/createOrder errors" + String.join(", ", errors));
                req.getRequestDispatcher("/view/order-create.jsp").forward(req, resp);
            } else {
                Boolean isCreated = orderController.addOrder(order);
                String message;
                if (isCreated) {
                    message = String.format("Order %s added!", order.getId());
                } else {
                    message = String.format("Error creating order %s!", order.getId());
                }
                LOG.debug(message);
                req.setAttribute("message", message);
            }
            req.getRequestDispatcher("/view/successful-operation.jsp").forward(req, resp);
        }
    }

    private List<String> validateOrder(Order order) {
        List<String> errors = new ArrayList<>();
        if (order.getId() <= 0) {
            errors.add("Order id cant be less or equal 0");
        } else {
            Order orderExists = orderController.getOrder(String.valueOf(order.getId()));
            if (orderExists != null) {
                errors.add(String.format("Order with such id=%s already exists", order.getPetId()));
            }
        }
        if (order.getPetId() <= 0) {
            errors.add("Pet id cant be less or equal 0");
        } else {
            Pet pet = new PetController().getPet(String.valueOf(order.getPetId()));
            if (pet == null) {
                errors.add(String.format("No pet with id=%s exists", order.getPetId()));
            }
        }
        if (order.getQuantity() <= 0) {
            errors.add("Quantity cant be less or equal 0");
        }
        return errors;
    }


    private String getAction(HttpServletRequest req) {
        final String requestURI = req.getRequestURI();
        final String requestPathWithServletContext = req.getContextPath() + req.getServletPath();
        return requestURI.substring(requestPathWithServletContext.length());
    }
}

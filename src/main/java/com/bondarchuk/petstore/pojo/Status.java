package com.bondarchuk.petstore.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;
import java.util.Optional;

@JsonIgnoreProperties(ignoreUnknown = true)
public enum Status {
    AVAILABLE("available"),
    PENDING("pending"),
    SOLD("sold");

    private String status;

    Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public static Optional<Status> getStatusValue(String value) {
        return Arrays.stream(Status.values())
                .filter(enumValue -> enumValue.getStatus().equals(value))
                .findAny();
    }
}

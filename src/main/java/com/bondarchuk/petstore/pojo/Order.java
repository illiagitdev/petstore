package com.bondarchuk.petstore.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Order {
    long id;
    long petId;
    int quantity;
    String shipDate;
    String status;
    boolean complete;
}

package com.bondarchuk.petstore.controller;

import com.bondarchuk.petstore.pojo.Category;
import com.bondarchuk.petstore.pojo.Pet;
import com.bondarchuk.petstore.pojo.Tags;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.apache.commons.fileupload.FileItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PetController {
    private static final Logger LOG = LogManager.getLogger(PetController.class);
    private static final String BASE_PET_URL = "http://petstore.swagger.io/v2/pet";
    private ObjectMapper mapper  = new ObjectMapper();
    private HttpUrls urls;
    private Responses responses;

    public PetController() {
        this.urls = new HttpUrls(BASE_PET_URL);
        this.responses = new Responses();
    }

    public List<Pet> petsByStatus(String status) {
        HttpUrl url = urls.getUrl("findByStatus", "status", status);
        try (Response response = responses.get(url)){
            validateResponse(response);
            return mapper.readValue(response.body().bytes(), new TypeReference<List<Pet>>() {
            });
        } catch (Exception e) {
            LOG.error(String.format("petsByStatus(%s) error:", status), e);
            return null;
        }
    }

    public Pet getPet(String id) {
        HttpUrl url = urls.getUrl(id);
        try (Response response = responses.get(url)){
            validateResponse(response);
            LOG.debug(String.format("getPet(%s) message:\n\t%s", id,response));
            return mapper.readValue(response.body().bytes(), new TypeReference<Pet>() {
            });
        } catch (Exception e) {
            LOG.error(String.format("getPet(%s) error:", id), e);
            return null;
        }
    }

    public String delete(String id) {
        HttpUrl url = urls.getUrl(id);
        try (Response response = responses.delete(url)){
            validateResponse(response);
            return null;
        } catch (IOException e) {
            LOG.error(String.format("delete(%s) error:", id), e);
            return "Error deleting pet ";
        }
    }

    public void addPet(Pet pet) {
        Response response = null;
        try {
            HttpUrl url = urls.getUrl();
            String petJson = mapper.writeValueAsString(pet);
            RequestBody requestBody = RequestBody.Companion.create(petJson, MediaType.get("application/json; charset=utf-8"));
            response = responses.post(url, requestBody);
            validateResponse(response);
            response.close();
            LOG.debug(String.format("addPet json:%s", petJson));
        } catch (Exception e) {
            if (response != null) {
                response.close();
            }
            LOG.error(String.format("addPet(%s-%s) error:", pet.getId(), pet.getName()), e);
        }
    }

    public void updatePet(String id, String name, String status) {
        Response response = null;
        try {
            HttpUrl url = urls.getUrl(id);
            RequestBody requestBody = new FormBody.Builder()
                    .add("name", name)
                    .add("status", status)
                    .build();
            response = responses.post(url, requestBody);
            validateResponse(response);
            response.close();
            LOG.debug(String.format("updatePet %s, %s, %s", id, name, status));
        } catch (Exception e) {
            if (response != null) {
                response.close();
            }
            LOG.error(String.format("updatePet(%s-%s) error:", id, name), e);
        }
    }

    public boolean checkId(long id) {
        HttpUrl url = urls.getUrl(String.valueOf(id));
        try (Response response = responses.get(url)){
            return response.isSuccessful();
        } catch (IOException e) {
            LOG.error(String.format("checkId(%s) error:", id), e);
            return false;
        }
    }

    public Boolean updatePet(Pet pet) {
        Response response = null;
        try {
            HttpUrl url = urls.getUrl();
            String petJson = mapper.writeValueAsString(pet);
            RequestBody requestBody = RequestBody.Companion.create(petJson, MediaType.get("application/json; charset=utf-8"));
            response = responses.put(url, requestBody);
            boolean successful = response.isSuccessful();
            response.close();
            return successful;
        } catch (IOException e) {
            LOG.error(String.format("updatePet(%s-%s) error:", pet.getId(), pet.getName()), e);
            return false;
        }
    }

    public void uploadFile(List<FileItem> items, String id) {
        try {
            for (FileItem item : items) {
                if (!item.isFormField()) {
                    HttpUrl url = urls.getUrl(id, "uploadImage");

                    RequestBody requestBody = new MultipartBody.Builder()
                            .setType(MultipartBody.FORM)
                            .addFormDataPart("Content-Type", "multipart/form-data")
                            .addFormDataPart("accept", "application/json")
                            .addFormDataPart(item.getName(), item.getFieldName(),
                                    RequestBody.create(item.get())).build();

                    LOG.debug(String.format("uploadFile(%s) %s :: %s", id, requestBody.contentType().toString(), requestBody.contentType()));
                    Response post = responses.post(url,requestBody);
                    post.close();
                }
            }
        } catch (Exception e) {
            LOG.error(String.format("uploadFile(%s) error:", id), e);
            throw new RuntimeException("Error when loading file " + e.getMessage());
        }
    }

    public Pet mapPet(HttpServletRequest req) {
        String id = req.getParameter("id");
        String name = req.getParameter("name").trim();
        String status = req.getParameter("status");
        String categoryId = req.getParameter("categoryId").trim();
        String categoryName = req.getParameter("categoryName").trim();
        String photoUrl = req.getParameter("photoUrl").trim();
        String tagName = req.getParameter("tagName").trim();
        String tadId = req.getParameter("tadId").trim();

        Category category = new Category();
        category.setId(Long.parseLong(categoryId));
        category.setName(categoryName);

        Tags tag = new Tags();
        tag.setId(Long.parseLong(tadId));
        tag.setName(tagName);
        List<Tags> tags = new ArrayList<>();
        tags.add(tag);

        Pet pet = new Pet();
        if (id != null) {
            pet.setId(Long.parseLong(id));
        }
        pet.setName(name);
        pet.setStatus(status);
        pet.setPhotoUrls(Collections.singletonList(photoUrl));
        pet.setCategory(category);
        pet.setTags(tags);
        return pet;
    }

    private void validateResponse(Response response) {
        if (response.isSuccessful()) {
            return;
        }
        if (response.code() >= 400 && response.code() <= 499) {
            throw new IllegalArgumentException(response.message());
        }
        if (response.code() >= 500) {
            throw new IllegalArgumentException(response.message());
        }
    }
}

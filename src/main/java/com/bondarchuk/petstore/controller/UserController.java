package com.bondarchuk.petstore.controller;

import com.bondarchuk.petstore.pojo.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class UserController {
    private static final Logger LOG = LogManager.getLogger(UserController.class);
    private static final String BASE_USER_URL = "http://petstore.swagger.io/v2/user";
    private ObjectMapper mapper = new ObjectMapper();
    private HttpUrls urls;
    private Responses responses;

    public UserController() {
        this.urls = new HttpUrls(BASE_USER_URL);
        this.responses = new Responses();
    }

    public User getUser(String username) {
        HttpUrl url = urls.getUrl(username);
        try (Response response = responses.get(url)){
            validateResponse(response);
            LOG.debug(String.format("getUser(%s) message:\n\t%s", username, response));
            return mapper.readValue(response.body().bytes(), new TypeReference<User>() {
            });
        } catch (Exception e) {
            LOG.error(String.format("getUser(%s) error: ", username), e);
            return null;
        }
    }

    public Boolean addUser(User user) {
        Response response = null;
        try {
            HttpUrl url = urls.getUrl();
            String userJson = mapper.writeValueAsString(user);
            RequestBody requestBody = RequestBody.Companion.create(userJson, MediaType.get("application/json; charset=utf-8"));
            response = responses.post(url, requestBody);
            validateResponse(response);
            response.close();
            LOG.debug(String.format("addUser message:%s", userJson));
            return true;
        } catch (Exception e) {
            if (response != null) {
                response.close();
            }
            LOG.error(String.format("Error addUser message:%s", user.getUsername()));
            return false;
        }
    }

    public Boolean deleteUser(String username) {
        HttpUrl url = urls.getUrl(username);
        try (Response response = responses.delete(url)){
            validateResponse(response);
            LOG.debug(String.format("deleteUser(%s) message:\n\t%s", username, response));
            return true;
        } catch (Exception e) {
            LOG.error(String.format("deleteUser(%s) error: ", username), e);
            return false;
        }
    }

    public Boolean updateUser(String username, User user) {
        Response response = null;
        try {
            HttpUrl url = urls.getUrl(username);
            String userJson = mapper.writeValueAsString(user);
            RequestBody requestBody = RequestBody.Companion.create(userJson, MediaType.get("application/json; charset=utf-8"));
            response = responses.put(url, requestBody);
            validateResponse(response);
            response.close();
            LOG.debug(String.format("updateUser message:%s", userJson));
            return true;
        } catch (Exception e) {
            if (response != null) {
                response.close();
            }
            LOG.error(String.format("updateUser(%s) error:", user.getUsername()), e);
            return false;
        }
    }

    public Boolean login(String username, String password) {
        User user = getUser(username);
        if (user != null) {
            HttpUrl url = urls.getUrl("login", "username", username, "password", password);
            try (Response response = responses.get(url)){
                validateResponse(response);
                return response.code() == 200;
            } catch (Exception e) {
                LOG.error(String.format("updateUser(%s) error:", user.getUsername()), e);
                return false;
            }
        }
        return false;
    }

    public void logout() {
        HttpUrl url = urls.getUrl("logout");
        try (Response response = responses.get(url)){
            validateResponse(response);
            LOG.debug(String.format("logout message:%s", response.message()));
        } catch (Exception e) {
            LOG.error("logout error:", e);
        }
    }

    public User mapUser(HttpServletRequest req) {
        String id = req.getParameter("id");
        String username = req.getParameter("username");
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String email = req.getParameter("email");
        String phone = req.getParameter("phone");
        String password = req.getParameter("password");

        User user = new User();
        user.setId(Long.parseLong(id));
        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPhone(phone);
        user.setPassword(password);
        user.setUserStatus(1);
        return user;
    }

    private void validateResponse(Response response) {
        if (response.isSuccessful()) {
            return;
        }
        if (response.code() >= 400 && response.code() <= 499) {
            throw new IllegalArgumentException(response.message());
        }
    }

    public Boolean isExists(String username) {
        User user = getUser(username);
        return user != null;
    }
}

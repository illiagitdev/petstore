package com.bondarchuk.petstore.controller;

import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Responses {
    private static final Logger LOG = LogManager.getLogger(Responses.class);
    private static OkHttpClient client;

    public Responses() {
        client = new OkHttpClient.Builder().connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .build();
    }

    public Response post(HttpUrl url, RequestBody requestBody) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        LOG.debug(String.format("post code=%s, url: %s,  requestBody: %s", response.code(), url, requestBody));
        return response;
    }

    public Response put(HttpUrl url, RequestBody requestBody) throws IOException {
        Request request = new Request.Builder()
                .put(requestBody)
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        LOG.debug(String.format("put code=%s, url: %s", response.code(), url));
        return response;
    }

    public Response get(HttpUrl url) throws IOException {
        Request request = new Request.Builder()
                .get()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        LOG.debug(String.format("get code=%s,  url: %s", response.code(), url));
        return response;
    }

    public Response delete(HttpUrl url) throws IOException {
        Request request = new Request.Builder()
                .delete()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        LOG.debug(String.format("delete code=%s,  url: %s", response.code(), url));
        return response;
    }
}

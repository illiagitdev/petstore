package com.bondarchuk.petstore.controller;

import okhttp3.HttpUrl;

public class HttpUrls {
    private String baseUrl;

    public HttpUrls(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public HttpUrl getUrl() {
        return HttpUrl.parse(baseUrl).newBuilder().build();
    }

    public HttpUrl getUrl(String pathSegment) {
        return HttpUrl.parse(baseUrl).newBuilder()
                .addPathSegment(pathSegment)
                .build();
    }

    public HttpUrl getUrl(String pathSegment1, String pathSegment2) {
        return HttpUrl.parse(baseUrl).newBuilder()
                .addPathSegment(pathSegment1)
                .addPathSegment(pathSegment2)
                .build();
    }

    public HttpUrl getUrl(String pathSegment, String queryParamName, String queryParamValue) {
        return HttpUrl.parse(baseUrl).newBuilder()
                .addPathSegment(pathSegment)
                .addQueryParameter(queryParamName, queryParamValue)
                .build();
    }

    public HttpUrl getUrl(String pathSegment, String queryParamName1, String queryParamValue1,
                                 String queryParamName2, String queryParamValue2) {
        return HttpUrl.parse(baseUrl).newBuilder()
                .addPathSegment(pathSegment)
                .addQueryParameter(queryParamName1, queryParamValue1)
                .addQueryParameter(queryParamName2, queryParamValue2)
                .build();
    }
}

package com.bondarchuk.petstore.controller;

import com.bondarchuk.petstore.pojo.Inventory;
import com.bondarchuk.petstore.pojo.Order;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class OrderController {
    private static final Logger LOG = LogManager.getLogger(OrderController.class);
    private static final String BASE_STORE_URL = "http://petstore.swagger.io/v2/store";
    private ObjectMapper mapper = new ObjectMapper();
    private HttpUrls urls;
    private Responses responses;

    public OrderController() {
        this.urls = new HttpUrls(BASE_STORE_URL);
        this.responses = new Responses();
    }

    public Order getOrder(String id) {
        HttpUrl url = urls.getUrl("order", id);
        try (Response response = responses.get(url)){
            validateResponse(response);
            LOG.debug(String.format("getOrder(%s) message:\n\t%s", id, response));
            response.close();
            return mapper.readValue(response.body().bytes(), new TypeReference<Order>() {});
        } catch (Exception e) {
            LOG.error(String.format("getOrder(%s) error: ", id), e);
            return null;
        }
    }

    public Boolean deleteOrder(String id) {
        HttpUrl url = urls.getUrl("order", id);
        try (Response response = responses.delete(url)){
            validateResponse(response);
            LOG.debug(String.format("deleteOrder(%s) message:\n\t%s", id, response));
            return true;
        } catch (Exception e) {
            LOG.error(String.format("deleteOrder(%s) error: ", id), e);
            return false;
        }
    }

    public Inventory getInventory() {
        HttpUrl url = urls.getUrl("inventory");
        try (Response response = responses.get(url)){
            validateResponse(response);
            LOG.debug(String.format("getInventory message:\n\t%s", response));
            return mapper.readValue(response.body().bytes(), new TypeReference<Inventory>() {
            });
        } catch (Exception e) {
            LOG.error("getInventory error: ", e);
            return null;
        }
    }

    public Boolean addOrder(Order order) {
        HttpUrl url = urls.getUrl("order");
        Response response = null;
        try {
            String orderJson = mapper.writeValueAsString(order);
            RequestBody requestBody = RequestBody.Companion.create(orderJson, MediaType.get("application/json; charset=utf-8"));
            response = responses.post(url, requestBody);
            validateResponse(response);
            response.close();
            LOG.debug(String.format("addOrder message:%s", orderJson));
            return true;
        } catch (Exception e) {
            LOG.error(String.format("Error addOrder message:%s", order.getId()));
            if (response != null) {
                response.close();
            }
            return false;
        }
    }

    private void validateResponse(Response response) {
        if (response.isSuccessful()) {
            return;
        }
        if (response.code() >= 400 && response.code() <= 499) {
            throw new IllegalArgumentException(response.message());
        }
    }

    public Order mapOrder(HttpServletRequest req) {
        String id = req.getParameter("id");
        String petId = req.getParameter("petId");
        String quantity = req.getParameter("quantity");
        String shipDate = req.getParameter("shipDate");
        String status = req.getParameter("status");
        String complete = req.getParameter("complete");

        Order order = new Order();
        order.setId(Long.parseLong(id));
        order.setPetId(Long.parseLong(petId));
        order.setQuantity(Integer.parseInt(quantity));
        order.setShipDate(shipDate);
        order.setStatus(status);
        if (complete.equals("true")) {
            order.setComplete(true);
        } else {
            order.setComplete(false);
        }
        return order;
    }
}

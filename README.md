### Pet Store
##### Requirements

**Sourse _http://petstore.swagger.io/v2/_**


*   write java-app for sending all available requests
*   allowed: 
    *   javascript and it's XMLHttpRequest &Fetch (*.html, *.js)
    *   java: console interface with all commands
    *   *java: servlets, *.html, *.jsp
    
**Technologies**


*   Java Core
*   OkHttp
*   Servlets
*   Html, JSP
*   Gradle